#!/bin/bash

set -eu
YO="node index.js"
export NM_BRID=`cat ../bc-target/blockchains/0/brid.txt`

$YO register-provider --cfg adm-conf.ini 03FCEDF62CD7AD5AE584E6187C011294C5D6177085657DFFE7644763E686403D10
$YO  enable-provider --cfg adm-conf.ini 03FCEDF62CD7AD5AE584E6187C011294C5D6177085657DFFE7644763E686403D10
$YO add-node --cfg prov2-conf.ini 035676109c54b9a16d271abeb4954316a40a32bcce023ac14c8e26e958aa68fba9 127.0.0.1 9871
sleep 3
$YO add-blockchain-signers --cfg adm-conf.ini $NM_BRID 035676109c54b9a16d271abeb4954316a40a32bcce023ac14c8e26e958aa68fba9
