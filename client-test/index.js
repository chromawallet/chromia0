const program = require("commander");
const fs = require("fs").promises;
const ini = require("ini");

const pcl = require("postchain-client");


async function makeGtxClient () {
    const config = ini.parse(await fs.readFile(
        program.cfg, 'utf-8')
                            );
   if (!config.nm_brid) config.nm_brid = process.env['NM_BRID'];

    const restClient = pcl.restClient.createRestClient(
        config.api_url, config.nm_brid
    );
    const gtxClient =   pcl.gtxClient.createClient(restClient,
            Buffer.from(config.nm_brid, 'hex'), []
                                                  );
    return {
        config,
      gtxClient,
      restClient,
      postTx: async (opgen) => {
        const tx = gtxClient.newTransaction([unhex(config.pubkey)]);
        opgen( tx );
        tx.sign(unhex(config.privkey), unhex(config.pubkey));
        await tx.postAndWaitConfirmation();
      }
    };
}

function unhex (s) {
    return Buffer.from(s, 'hex');
}

program.option("--cfg <path>");

async function postTx( client,  opgen ) {
  const {gtxClient, config} = client;
  
}
                        
program.command('register-provider <pubkey>')
  .action( async (pubkey) => {
    const client = await makeGtxClient();
    await client.postTx( tx => tx.addOperation("register_provider",
                                        pubkey));
  });

program.command('enable-provider <pubkey>')
  .action( async (pubkey) => {
    const client = await makeGtxClient();
    const providerID = await client.restClient.query( 'get_provider', {pubkey} );    
    await client.postTx( tx => tx.addOperation("enable_provider",
                                        providerID));
  });

program.command('add-node <node_pubkey> <host> <port>')
  .action( async (node_pk, host, port) => {
    const client = await makeGtxClient();
    const providerID = await client.restClient.query(
      'get_provider',   {pubkey: client.config.pubkey} );
    await client.postTx( tx => tx.addOperation(
      "add_node",
      providerID, unhex(node_pk), host, parseInt(port)));
  });

program.command('add-blockchain-signers <blockchain> <signers>')
  .action( async (blockchain, signers) => {
    const client = await makeGtxClient();
    const node_list = await Promise.all(signers.split(",").map(
      node_pk => client.restClient.query('get_node', { pubkey: node_pk })
    ));
    const blockchainID = await client.restClient.query(
      'get_blockchain',   {rid: blockchain} );
    await client.postTx(
      tx => tx.addOperation(
        "add_blockchain_signers",
        blockchainID, node_list)
    );
  });


program.command("add-blockchain <what> <nodes>")
    .action( async (bcconf, nodes) => {
      try {
        const client = await makeGtxClient();

        const node_list = await Promise.all(nodes.split(",").map(
          node_pk => client.restClient.query('get_node', { pubkey: node_pk })
        ));
        
        const data = await fs.readFile(bcconf);

        await client.postTx ( tx =>
                       tx.addOperation("add_blockchain",
                                       data, node_list
                                      ));
        } catch (e) {
            console.log("Got error:", e.stack);
        }
    });

program.command("get-peers").action(
  async () => {
    const client = await makeGtxClient();
    console.log(
      await client.restClient.query("nm_get_peer_infos", { current_height: 0}));

  });

program.parse(process.argv);
