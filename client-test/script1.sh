#!/bin/bash

set -eu
YO="node index.js"
export NM_BRID=`cat ../bc-target/blockchains/0/brid.txt`

$YO register-provider --cfg adm-conf.ini 03962AB49BC8D056C56A405DEFDA2448DE3A6AF65E6EA84019EE551A3526D0ADB0
$YO  enable-provider --cfg adm-conf.ini 03962AB49BC8D056C56A405DEFDA2448DE3A6AF65E6EA84019EE551A3526D0ADB0
sleep 1
$YO add-node --cfg prov-conf.ini 0350fe40766bc0ce8d08b3f5b810e49a8352fdd458606bd5fafe5acdcdc8ff3f57 127.0.0.1 9870
sleep 4
$YO add-blockchain --cfg adm-conf.ini  ../bc-target/blockchains/0/0.gtv 0350fe40766bc0ce8d08b3f5b810e49a8352fdd458606bd5fafe5acdcdc8ff3f57
