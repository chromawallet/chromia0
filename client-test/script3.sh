#!/bin/bash

set -eu

YO="node index.js"
export NM_BRID=`cat ../bc-target/blockchains/0/brid.txt`

$YO add-blockchain --cfg adm-conf.ini  ../bc-target/blockchains/100/0.gtv 0350fe40766bc0ce8d08b3f5b810e49a8352fdd458606bd5fafe5acdcdc8ff3f57,035676109c54b9a16d271abeb4954316a40a32bcce023ac14c8e26e958aa68fba9
