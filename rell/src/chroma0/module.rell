@mount('')
module;

import ft3.ft3_basic;
import ft3_account: ft3.account;
import ft3_core: ft3.core;

struct module_args {
    admin: pubkey;
}

entity provider { key pubkey; }

entity node {
	index provider;
	key pubkey;
	mutable active: boolean = true;
	mutable host: text;
	mutable port: integer;
	mutable last_updated: timestamp;
}

object node_list {
	mutable last_update: timestamp = 0;
}

entity provider_state {
	key provider;
	mutable name: text;
	mutable active: boolean;
	mutable beneficiary: ft3_account.account; 	 
}

entity blockchain { key rid: byte_array; }

entity blockchain_added { key blockchain; key transaction; }

entity blockchain_configuration {
	key blockchain, height: integer;
	data: byte_array;
}

entity blockchain_signer_node { key blockchain, node; }
entity blockchain_replica_node { key blockchain, node; }

function require_admin() { 	require(is_signer(chain_context.args.admin)); }

function create_simple_ft3_account(pubkey): ft3_account.account {
	val account_id = ft3_account.create_account_with_auth(
		ft3_account.auth_descriptor(
			"C",
			[pubkey],
			[pubkey.to_gtv()]
		)	
	);
	return ft3_account.account @ { account_id };
}

operation init () {
	require_admin();
	ft3_core.register_asset("CHR", chain_context.blockchain_rid);
	create_simple_ft3_account(chain_context.args.admin);
}


operation register_provider (pubkey) {
	require_admin();
	val provider = create provider (pubkey);
	val account = create_simple_ft3_account(pubkey);
	create provider_state (provider, name="", 
		active=false,
		beneficiary=account
	);
	create activity_counter (provider);
}

function require_provider_auth (provider) { require( is_signer(provider.pubkey)); }

operation update_provider_data (provider, name?, beneficiary_account_id: byte_array?) {
	require_provider_auth(provider);
	if (exists(name)) {
		update provider_state @ {provider} (
			.name = name
		);		
	}
	if (exists(beneficiary_account_id)) {
		update provider_state @ {provider} (
			.beneficiary = ft3_account.account @ { .id == beneficiary_account_id }
		);
	} 
}

operation add_node (provider, node_pubkey: pubkey, host: text, port: integer) {
	require_provider_auth(provider);
	val existing_node = node @? { node_pubkey }; 
	if (exists(existing_node)) {
		require(existing_node.provider == provider);
		update existing_node (
			.active = true,
			.host = host, 
			.port = port,
			.last_updated = op_context.last_block_time
		);
	} else {
		create node (provider, node_pubkey, host, port,
			last_updated = op_context.last_block_time
		);
	}
	node_list.last_update = op_context.last_block_time;
}

operation add_replica (provider?, blockchain, node) {
	if (exists(provider)) {
		require_provider_auth(provider);
		require( node.provider == provider );		
	} else {
		require_admin();			
	}
	
	require(empty(blockchain_signer_node @? { blockchain, node }));
	require(empty(blockchain_replica_node @? { blockchain, node }));
	create blockchain_replica_node ( blockchain, node );
}

operation remove_replica (provider?, blockchain, node) {
	if (exists(provider)) {
		require_provider_auth(provider);
		require( node.provider == provider );		
	} else {
		require_admin();			
	}
	delete blockchain_replica_node @ { blockchain, node };
}


operation remove_node (provider, node_pubkey: pubkey) {
	require_provider_auth(provider);
	val node_to_remove = node @ { node_pubkey };
	node_to_remove.active = false;
	for (blockchain in blockchain_signer_node @* { .node == node_to_remove } .blockchain) {
		update_configuration(blockchain);
	}
	node_list.last_update = op_context.last_block_time;
}

operation enable_provider (provider) {
	require_admin();
	update provider_state @ { provider } ( .active = true );
}

operation disable_provider (provider) {
	require_admin();
	update provider_state @ { provider } ( .active = false );
	update node @* { provider } ( .active = false );
	for (blockchain in blockchain_signer_node @* { .node.provider == provider } (.blockchain)) {
		update_configuration(blockchain);
	}
	node_list.last_update = op_context.last_block_time;
}

function get_last_height (blockchain): integer {
	if (blockchain.rid == chain_context.blockchain_rid) {
		return (block @? {} (@sort_desc .block_height) limit 1) ?: -1;
	} else {
		return (anchored_block @? { blockchain } (@sort_desc .height) limit 1) ?: -1;
	}
}

function patch_configuration_signers(blockchain, data: byte_array): byte_array {
    val config_dict = map<text, gtv>.from_gtv(gtv.from_bytes(data));
	val signers = blockchain_signer_node @* { blockchain } (@sort .node.pubkey);
	require(signers.size() > 0);
	// do not write new configuration when size is 0 since it's impossible to recover
	// from that
    config_dict["signers"] = signers.to_gtv();     
	return config_dict.to_gtv().to_bytes();
}


function update_configuration (blockchain) {
	val last_config = blockchain_configuration @ { blockchain } (@sort_desc .height, .data) limit 1;
    delete blockchain_signer_node @* { blockchain, .node.active == false};
    val last_height = get_last_height(blockchain);
    val updated_config = patch_configuration_signers( blockchain, last_config.data );
    
    create blockchain_configuration (
        blockchain,
        height = last_height + 5,
        data = updated_config
    );
}

operation add_blockchain_signers (blockchain, add_signers: list<node>) {
	require_admin();
	for (signer in add_signers) {
		require(signer.active);
		create blockchain_signer_node(blockchain, signer);
	}
	update_configuration(blockchain);	
}

operation remove_blockchain_signers (blockchain, remove_signers: list<node>) {
	require_admin();
	for (signer in remove_signers) {
		delete blockchain_signer_node @? {blockchain, signer};
	}
	update_configuration(blockchain);
}

operation add_blockchain ( config_data: byte_array, initial_nodes: list<node> ) {
	require_admin();
	require(initial_nodes.size() > 0);
	val config = map<text, gtv>.from_gtv(gtv.from_bytes(config_data));
	val signers = list<pubkey>();
	for (node in initial_nodes) {
		signers.add(node.pubkey);
	}
	config["signers"] = signers.to_gtv();
	val blockchain_rid = config.hash();
	print("Added blockchain", blockchain_rid);
	val blockchain = create blockchain ( blockchain_rid );
	create blockchain_configuration (blockchain, 0, config.to_gtv().to_bytes());
	for (node in initial_nodes) {
		create blockchain_signer_node (blockchain, node);
	}
	create blockchain_added (blockchain, op_context.transaction);
}

operation add_configuration (blockchain, config_data: byte_array, height: integer) {
	require_admin();
	require(get_last_height(blockchain) < height);
	require(empty(blockchain_configuration @? { blockchain, .height > height } limit 1));
	create blockchain_configuration(
		blockchain,
		height,
		data = patch_configuration_signers(blockchain, config_data)
	);
}

operation stop_blockchain (blockchain, remove_replicas: boolean) {
	require_admin();
	
	// NOTE: we do not update blockchain configuration, making it possible to 
	// restart blockchain later. Data such as anchored blocks is not removed.
	delete blockchain_signer_node @* { blockchain };
	if (remove_replicas) {
		delete blockchain_replica_node @* { blockchain };
	} 
}
